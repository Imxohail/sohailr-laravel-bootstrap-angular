@extends('layouts.app')

@section('content')

@if(Session::has('success'))
  <div class="alert-box success">
    <h2>{!! Session::get('success') !!}</h2>
  </div>
@endif

<div class="container">
    <div class="row">
      <form action="{{ url('/uploader') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
          {!! csrf_field() !!}

          <!-- User Name -->
          <div class="form-group">
              <label for="name" class="col-sm-3 control-label">Upload File</label>

              <div class="col-sm-6">
                  <input type="file" name="fileToUpload" id="fileToUpload" class="form-control">
              </div>
          </div>

          <!-- Edit User Button -->
          <div class="form-group">
              <div class="col-sm-offset-3 col-sm-6">
                  <button type="submit" class="btn btn-default">
                      <i class="fa fa-plus"></i> Submit File
                  </button>
              </div>
          </div>
      </form>
    </div>
    <div class="container">
      @if ($output)
        {{$output}}
      @endif
    </div>
</div>
@endsection
