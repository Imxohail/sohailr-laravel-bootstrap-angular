<style>
.table td, th{
   text-align: center;
}
</style>
<div class="modal-dialog" ng-app="application" ng-controller="controller">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h2 class="modal-title">Billing Information</h2>
    </div>
    <div class="modal-body">

      <div>
        <div class="reg-header">
            <h4 class="modal-title">Order Information</h4>
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <tr>
                  <th>Item</th>
                  <th>Quantity</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="row in cart">
                  <td class="text-danger">@{{row.name | ucfirst}}</td>
                  <td class="text-info"><p>@{{row.qty}}</p></td>
                  <td>@{{row.price}}</td>
                </tr>
                <tr>
                  <td colspan="2"></td>
                  <td><p class="text-primary bg-success"><strong>Grand Total: @{{grandTotal}}<strong></p></td>
                </tr>
              </tbody>
            </table>
        </div>

        <hr>

          <form class="reg-page" name="paypalForm" action="/paypal" method="POST">
              {!! csrf_field() !!}
              <label>First Name <span class="color-red">*</span></label>
              <input type="text" name="firstname" class="form-control margin-bottom-20" required on-change-validate>
              <div ng-show="firstname_error">
                <span class="text text-danger">Invalid First Name</span>
              </div>

              <label>Last Name <span class="color-red">*</span></label>
              <input type="text" name="lastname" class="form-control margin-bottom-20" required on-change-validate>
              <div ng-show="lastname_error">
                <span class="text text-danger">Invalid Last Name</span>
              </div>

              <label>Billing Address <span class="color-red">*</span></label>
              <input type="text" name="address" class="form-control margin-bottom-20" required>

              <hr>

              <div class="row">
                  <div class="col-lg-6 checkbox">

                  </div>
                  <div class="col-lg-6 text-right">
                      <button class="btn" style="background: #5fb611;" type="submit" ng-disabled="checkout">Checkout</button>
                  </div>
              </div>
          </form>
        </div>
      <!-- <div class="modal-footer">

      </div> -->
    </div>

  </div>
</div>
