// Diretive for the validation of Form
// @param: string
// @return: string
app.directive("onChangeValidate", function($parse){
  return {
    restrict: 'A',

    link: function(scope, elem, attrs){
      elem.bind('change', function() {
         selectedElement  = elem.attr('name');
         field            = $parse(selectedElement+"_error");
         string           = elem.val();
         string           = string.replace(/ /g, ''); // strips spaces

        // If input contains Alphabets only!!
         if(!(/^[a-zA-Z]*$/.test(string))){
           scope.$apply(function() {
              field.assign(scope, true); //Display error
              scope.checkout = true; //Disable submit
           });
         }
         else {
           scope.$apply(function() {
              field.assign(scope, false); //Hide error
              scope.checkout = false; //Enable submit
           });
         }
      });
    }
  };
});
