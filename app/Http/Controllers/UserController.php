<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use Session;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;
use SoapBox\Formatter\Formatter;
use App\Http\Requests;

class UserController extends Controller
{

    public function __construct(){
      $this->middleware('auth');
    }


    public function index(){
      $admin = Auth::user()->admin;

      $users = DB::table('users')->get();
      return view('users.index', ['users' => $users, 'privilidge' => $admin]);
    }

    public function edit($id = NULL){
      if($id){
        $user = DB::table('users')->find($id);
        return view('users.edit', ['user' => $user]);
      }
      else {
        return view('users.edit');
      }
    }

    public function create(){
      return view('users.create');
    }

    public function delete(Request $request){
      DB::table('users')->where('id', $request->id)->delete();
      return redirect('/users');
    }

    public function update(Request $request){
      $this->validate($request, [
        'name' => 'required|max:255',
      ]);

      $this->validate($request, [
        'email' => 'required|max:255',
      ]);
      if($request->rights == NULL)
        $rights = 0;
      else {
        $rights = 1;
      }
      if($request->id)
        DB::table('users')
              ->where('id', $request->id)
              ->update(['name' => $request->name, 'email' => $request->email, 'admin' => $rights]);
      else {
        DB::table('users')->insert(
            ['name' => $request->name, 'email' => $request->email, 'admin' => $rights]
        );
      }
      //var_dump($request->rights);
      return redirect('/users');
    }

    public function uploader(Request $request)
    {
      // $array = ['test' => 'sohail', 'music' => 'jazz'];

      if (Input::file('fileToUpload')->isValid()) {
        $destinationPath = 'tests'; // upload path
        $extension = Input::file('fileToUpload')->getClientOriginalExtension(); // getting image extension
        $fileName = rand(11111,99999).'.'.$extension; // renaming
        Input::file('fileToUpload')->move($destinationPath, $fileName); // uploading file to given path
        // sending back with message
        Session::flash('success', 'Upload successfully');

        $fileContent = file_get_contents('tests/'.$fileName);

        $formatter = Formatter::make($fileContent, Formatter::XML);
        $json   = $formatter->toYaml();
        //return redirect('/parser',['fileContent' => $fileContent]);
        return view('fileupload',['output' => $json]);
      }
    }

    public function InputForm($fileContent = NULL)
    {
      return view('fileupload',['output' => $fileContent]);
    }
}
