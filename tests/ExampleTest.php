<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->visit('/')
              ->see('Welcome');

        $this->visit('/register')
            ->type('test', 'name')
            ->type('taylor54@gmail.com', 'email')
            ->type('taylor123', 'password')
            ->type('taylor123', 'password_confirmation')
            ->press('Register')
            ->seePageIs('/');

        $this->visit('/logout')
            ->seePageIs('/');

        $this->visit('/login')
              ->type('taylor54@gmail.com', 'email')
              ->type('taylor123', 'password')
              ->check('remember')
              ->press('Login')
              ->seePageIs('/');

        $this->visit('/login')
              ->click('Forgot Your Password?')
              ->seePageIs('/password/reset');

        $this->visit('/password/reset')
              ->type('taylor54@gmail.com', 'email')
              ->press('Send Password Reset Link')
              ->seePageIs('/test');
    }
}
