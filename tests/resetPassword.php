<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class resetPassword extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
      $this->visit('/login')
            ->type('shazaib.khadim@gmail.com', 'email')
            ->type('test123', 'password')
            ->check('remember')
            ->press('Login')
            ->seePageIs('/');

    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample1()
    {
      // $this->visit('/password/reset')
      //       ->type('shazaib.khadim@gmail.com', 'email')
      //       // ->type('test123', 'password')
      //       // ->check('remember')
      //       ->press('Login')
      //       ->seePageIs('/test');

      $this->visit('/register')
          ->type('test', 'name')
          ->type('taylor@gmail.com', 'email')
          ->type('taylor', 'password')
          ->type('taylor', 'password_confirmation')
          ->press('Register')
          ->seePageIs('/');

    }
}
